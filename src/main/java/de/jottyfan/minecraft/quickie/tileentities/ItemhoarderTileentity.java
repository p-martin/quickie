package de.jottyfan.minecraft.quickie.tileentities;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.jottyfan.minecraft.quickie.QuickieMod;
import de.jottyfan.minecraft.quickie.util.QuickieBlocks;
import net.minecraft.block.Block;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;

/**
 * 
 * @author jotty
 *
 */
public class ItemhoarderTileentity extends TileEntity implements ITickableTileEntity, IInventory {
	private static final Logger LOGGER = LogManager.getLogger(ItemhoarderTileentity.class);

	private final NonNullList<ItemStack> stacks;

	public ItemhoarderTileentity() {
		super(ForgeRegistries.TILE_ENTITIES.getValue(new ResourceLocation(QuickieMod.MODID, "itemhoardertileentity")));
		stacks = NonNullList.withSize(255, ItemStack.EMPTY);
	}

	@Override
	public void remove() {
		for (ItemStack stack : stacks) {
			Block.spawnAsEntity(getWorld(), getPos(), stack);
		}
		// also, spawn the item hoarder itself
		Block.spawnAsEntity(getWorld(), getPos(), new ItemStack(QuickieBlocks.ITEMHOARDER));
		super.remove();
	}

	private final Integer getNextFreeEmptyStackPosition() {
		Integer emptyStackPosition = null;
		Iterator<ItemStack> it = stacks.iterator();
		Integer index = 0;
		while (emptyStackPosition == null && it.hasNext()) {
			ItemStack s = it.next();
			if (s.isEmpty()) {
				emptyStackPosition = index;
			}
			index++;
		}
		return emptyStackPosition;
	}

	@Override
	public void tick() {
		BlockPos pos = getPos();
		World world = getWorld();
		// list all items around here
		AxisAlignedBB aabb = new AxisAlignedBB(pos).grow(4);
		List<ItemEntity> itemEntities = world.getEntitiesWithinAABB(ItemEntity.class, aabb);
		boolean dirty = false;
		for (ItemEntity itemEntity : itemEntities) {
			LOGGER.debug("found itemstack @ ({}:{}:{})", pos.getX(), pos.getY(), pos.getZ());
			if (itemEntity.isAlive()) {
				ItemStack stack = itemEntity.getItem();
				if (stack != null) {
					Integer emptyStackPosition = getNextFreeEmptyStackPosition();
					if (emptyStackPosition == null) {
						LOGGER.error("no empty itemstack found in itemhoarder");
					} else {
						stacks.set(emptyStackPosition, stack);
						LOGGER.debug("added stack of {} to item hoarder", stack.getItem().getRegistryName().getPath());
						itemEntity.remove();
						LOGGER.debug("removed stack of {} from surface", stack.getItem().getRegistryName().getPath());
						dirty = true;
					}
				}
			} else {
				LOGGER.debug("stack of {} is still added to one itemhoarder, aborting adding it again...",
						itemEntity.getItem().getItem().getRegistryName().getPath());
			}
		}
		if (dirty) {
			resortStacks();
		}
	}

	public NonNullList<ItemStack> getStacks() {
		return stacks;
	}

	/**
	 * resort item stacks to be more informative in the summary
	 */
	private void resortStacks() {
		Map<ITextComponent, ItemStack> map = new HashMap<>();
		Integer clearAmount = stacks.size();
		for (ItemStack stack : stacks) {
			ItemStack found = map.get(stack.getDisplayName());
			if (found == null) {
				found = stack;
				map.put(stack.getDisplayName(), found);
			} else {
				found.setCount(found.getCount() + stack.getCount());
			}
		}
		for (Integer i = 0; i < clearAmount; i++) {
			stacks.set(i, ItemStack.EMPTY);
		}
		Integer index = 0;
		for (ItemStack stack : map.values()) {
			stacks.set(index++, stack);
		}
	}

	/**
	 * get summary of content
	 * 
	 * @return string with content information
	 */
	public String getSummary() {
		StringBuilder buf = new StringBuilder();
		Iterator<ItemStack> i = stacks.iterator();
		while (i.hasNext()) {
		  ItemStack stack = i.next();
			if (!Items.AIR.equals(stack.getItem())) {
				buf.append(stack.getCount()).append("x ");
				ITextComponent itc = stack.getItem().getDisplayName(stack);
				buf.append(itc.getFormattedText().replace("[", "").replace("]", ""));
				buf.append(i.hasNext() ? ", " : "");
			}
		}
		return buf.toString();
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		if (compound.contains("stacks")) {
			INBT nbtTagList = compound.get("stacks");
			if (nbtTagList == null) {
				LOGGER.warn("stacks is null in taglist");
			} else if (nbtTagList instanceof ListNBT) {
				ListNBT list = (ListNBT) nbtTagList;
				for (Integer i = 0; i < stacks.size(); i++) {
					stacks.set(i, ItemStack.EMPTY);
				}
				Iterator<INBT> i = list.iterator();
				LOGGER.debug("seeking list of nbt tags for item stacks");
				while (i.hasNext()) {
					INBT next = i.next();
					if (next instanceof CompoundNBT) {
						CompoundNBT c = (CompoundNBT) next;
						ItemStack stack = ItemStack.read(c);
						Integer emptyStackPosition = getNextFreeEmptyStackPosition();
						if (emptyStackPosition == null) {
							LOGGER.error("no empty itemstack found in itemhoarder");
						} else {
							stacks.set(emptyStackPosition, stack);
							if (world.isRemote) {
								ITextComponent itc = stack.getItem().getName();
								LOGGER.debug("found {}x {}", stack.getCount(), itc.getFormattedText());
							}
						}
					} else {
						LOGGER.warn("found a non NBTTagCompound in the list: {}",
								next == null ? "null" : next.getClass().getSimpleName());
					}
				}
			} else {
				LOGGER.warn("found a non NBTTagList in the tags with name stacks: {}", nbtTagList.getClass().getSimpleName());
			}
		} else {
			LOGGER.warn("no tag with key stacks");
		}
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		ListNBT list = new ListNBT();
		for (ItemStack stack : stacks) {
			list.add(stack.write(new CompoundNBT()));
		}
		compound.put("stacks", list);
		return super.write(compound);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.write(new CompoundNBT());
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.read(pkt.getNbtCompound());
	}

	@Override
	public void clear() {
		stacks.clear();
	}

	@Override
	public int getSizeInventory() {
		return stacks.size();
	}

	@Override
	public boolean isEmpty() {
		return stacks.parallelStream().allMatch(ItemStack::isEmpty);
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return stacks.get(index);
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		final ItemStack itemstack = ItemStackHelper.getAndSplit(stacks, index, count);
		if (!itemstack.isEmpty()) {
			markDirty();
		}
		return itemstack;
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		return ItemStackHelper.getAndRemove(stacks, index);
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if (!stack.getItem().equals(Items.AIR)) {
			stacks.set(index, stack);
			if (stack.getCount() > getInventoryStackLimit()) {
				stack.setCount(getInventoryStackLimit());
			}
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 1000000;
	}

	@Override
	public boolean isUsableByPlayer(PlayerEntity player) {
		return false;
	}
}
