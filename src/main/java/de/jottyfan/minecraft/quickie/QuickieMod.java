package de.jottyfan.minecraft.quickie;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.jottyfan.minecraft.quickie.event.EventAutoSapling;
import de.jottyfan.minecraft.quickie.event.EventBlockBreak;
import de.jottyfan.minecraft.quickie.proxy.ClientProxy;
import de.jottyfan.minecraft.quickie.proxy.IProxy;
import de.jottyfan.minecraft.quickie.proxy.ServerProxy;
import de.jottyfan.minecraft.quickie.tileentities.ItemhoarderSupplier;
import de.jottyfan.minecraft.quickie.util.QuickieBlocks;
import de.jottyfan.minecraft.quickie.util.QuickieItems;
import de.jottyfan.minecraft.quickie.world.OreGenerator;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;

/**
 * 
 * @author jotty
 *
 */
@Mod("quickie")
public class QuickieMod {
	private static final Logger LOGGER = LogManager.getLogger(QuickieMod.class);

	public static final String MODID = "quickie";

	public static IProxy proxy = DistExecutor.runForDist(() -> ClientProxy::new, () -> ServerProxy::new);

	public QuickieMod() {
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		IEventBus bus = MinecraftForge.EVENT_BUS;
		bus.register(this);
		bus.register(new EventAutoSapling());
		bus.register(new EventBlockBreak());
		proxy.registerClientEventFaceBlock(bus);
	}

	private void setup(final FMLCommonSetupEvent event) {
		IForgeRegistry<Biome> forgeBiomes = ForgeRegistries.BIOMES;
		new OreGenerator().registerOres(forgeBiomes.getValues());
	}

	@SubscribeEvent
	public void onServerStarting(FMLServerStartingEvent event) {
	}

	@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class RegistryEvents {
		@SubscribeEvent
		public static void onBlocksRegistry(final RegistryEvent.Register<Block> event) {
			for (Block block : QuickieBlocks.BLOCKS) {
				event.getRegistry().register(block);
				LOGGER.debug("registered block {}", block.getRegistryName().getPath());
			}
		}

		@SubscribeEvent
		public static void onItemsRegistry(final RegistryEvent.Register<Item> event) {
			for (Item item : QuickieItems.ITEMS) {
				event.getRegistry().register(item);
				LOGGER.debug("registered item {}", item.getRegistryName().getPath());
			}
			// the blocks as items
			for (Block block : QuickieBlocks.BLOCKS) {
				BlockItem blockItem = new BlockItem(block, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS));
				blockItem.setRegistryName(block.getRegistryName());
				event.getRegistry().register(blockItem);
				LOGGER.debug("registered block item {}", blockItem.getRegistryName().getPath());
			}

		}

		@SubscribeEvent
		public static void onTileEntityRegistry(final RegistryEvent.Register<TileEntityType<?>> event) {
			TileEntityType<?> itemhoarderTileEntity = TileEntityType.Builder
					.create(new ItemhoarderSupplier(), QuickieBlocks.ITEMHOARDER).build(null);
			itemhoarderTileEntity.setRegistryName(QuickieMod.MODID, "itemhoardertileentity");
			event.getRegistry().register(itemhoarderTileEntity);
			LOGGER.debug("registered tileentity {}", itemhoarderTileEntity.getRegistryName().getPath());
		}
	}
}
