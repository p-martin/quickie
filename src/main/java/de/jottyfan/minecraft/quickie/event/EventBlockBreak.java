package de.jottyfan.minecraft.quickie.event;

import java.util.ArrayList;
import java.util.List;

import de.jottyfan.minecraft.quickie.items.HarvestRange;
import de.jottyfan.minecraft.quickie.items.ToolRangeable;
import de.jottyfan.minecraft.quickie.util.QuickieItems;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * 
 * @author jotty
 *
 */
public class EventBlockBreak {

	private enum BlockBreakDirection {
		UPWARDS, ALL;
	}

	@SubscribeEvent
	public void doBreakBlock(BlockEvent.BreakEvent event) {
		PlayerEntity player = event.getPlayer();
		ItemStack mainHandItemStack = player.getHeldItemMainhand();
		if (mainHandItemStack != null) {
			Item item = mainHandItemStack.getItem();
			if (item instanceof ToolRangeable) {
				CompoundNBT nbt = mainHandItemStack.getTag();
				Integer level = nbt != null ? nbt.getInt("level") : 0;
				ToolRangeable tool = (ToolRangeable) item;
				BlockPos pos = event.getPos();
				World world = event.getWorld().getWorld();
				BlockState blockState = world.getBlockState(pos);
				Block block = blockState.getBlock();
				handleRangeableTools(tool, level, world, block, pos, player);
			}
		}
	}

	/**
	 * hande the rangeable tools break event
	 * 
	 * @param tool
	 *          the tool that has been used
	 * @param world
	 *          the world
	 * @param block
	 *          the block to break
	 * @param pos
	 *          the position of the current block
	 * @param player
	 *          the current player
	 */
	private void handleRangeableTools(ToolRangeable tool, Integer level, World world, Block currentBlock, BlockPos pos,
			PlayerEntity player) {
		List<Block> validBlocks = tool.getBlockList(currentBlock);
		HarvestRange range = tool.getRange();
		if (range != null) {
			range = range.addXYZ(level);
		}
		if (QuickieItems.AXE_SPEEDPOWDER.equals(tool)) {
			breakBlockRecursive(new ArrayList<>(), world, validBlocks, pos, tool, range, BlockBreakDirection.UPWARDS, player);
		} else if (QuickieItems.PICKAXE_SPEEDPOWDER.equals(tool)) {
			breakBlockRecursive(new ArrayList<>(), world, validBlocks, pos, tool, range, BlockBreakDirection.ALL, player);
		} else if (QuickieItems.SHOVEL_SPEEDPOWDER.equals(tool)) {
			breakBlockRecursive(new ArrayList<>(), world, validBlocks, pos, tool, range, BlockBreakDirection.ALL, player);
		}
	}

	/**
	 * break block recursively;
	 * 
	 * @param visitedBlocks
	 *          the positions of visited blocks
	 * @param world
	 *          the world
	 * @param validBlocks
	 *          the blocks to break
	 * @param tool
	 *          the tool used
	 * @param range
	 *          the range left over
	 * @param pos
	 *          the position
	 * @param blockBreakDirection
	 *          the direction for the recursive call
	 * @param player
	 *          the player
	 */
	private void breakBlockRecursive(List<String> visitedBlocks, World world, List<Block> validBlocks, BlockPos pos,
			ToolRangeable tool, HarvestRange range, BlockBreakDirection blockBreakDirection, PlayerEntity player) {
		if (visitedBlocks.contains(pos.toString())) {
			return; // reduce loops
		} else if (validBlocks == null) {
			return; // reduce loops
		} else {
			visitedBlocks.add(pos.toString());
		}
		BlockState blockState = world.getBlockState(pos);
		if (tool.canBreakNeigbbors(blockState)) {
			Block currentBlock = blockState.getBlock();
			if (validBlocks.contains(currentBlock)) {
				world.setBlockState(pos, Blocks.AIR.getDefaultState());
				Block.spawnDrops(blockState, world, pos);
				if (range == null || range.getxRange() > 1 || range.getyRange() > 1 || range.getzRange() > 1) {
					HarvestRange nextRadius = range == null ? null : range.addXYZ(-1);
					breakBlockRecursive(visitedBlocks, world, validBlocks, pos.north(), tool, nextRadius, blockBreakDirection,
							player);
					breakBlockRecursive(visitedBlocks, world, validBlocks, pos.north().east(), tool, nextRadius,
							blockBreakDirection, player);
					breakBlockRecursive(visitedBlocks, world, validBlocks, pos.north().west(), tool, nextRadius,
							blockBreakDirection, player);
					breakBlockRecursive(visitedBlocks, world, validBlocks, pos.south(), tool, nextRadius, blockBreakDirection,
							player);
					breakBlockRecursive(visitedBlocks, world, validBlocks, pos.south().east(), tool, nextRadius,
							blockBreakDirection, player);
					breakBlockRecursive(visitedBlocks, world, validBlocks, pos.south().west(), tool, nextRadius,
							blockBreakDirection, player);
					breakBlockRecursive(visitedBlocks, world, validBlocks, pos.east(), tool, nextRadius, blockBreakDirection,
							player);
					breakBlockRecursive(visitedBlocks, world, validBlocks, pos.west(), tool, nextRadius, blockBreakDirection,
							player);

					breakBlockRecursive(visitedBlocks, world, validBlocks, pos.up(), tool, nextRadius, blockBreakDirection,
							player);

					if (BlockBreakDirection.ALL.equals(blockBreakDirection)) {
						breakBlockRecursive(visitedBlocks, world, validBlocks, pos.down(), tool, nextRadius, blockBreakDirection,
								player);
					}
				}
			}
		}
	}
}
