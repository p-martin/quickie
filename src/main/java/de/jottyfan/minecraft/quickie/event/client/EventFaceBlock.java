package de.jottyfan.minecraft.quickie.event.client;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import de.jottyfan.minecraft.quickie.items.ItemConstruction2;
import de.jottyfan.minecraft.quickie.items.construction.ConstructionBean;
import de.jottyfan.minecraft.quickie.items.construction.EnumExtreme;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * 
 * @author jotty
 *
 */
@OnlyIn(Dist.CLIENT)
public class EventFaceBlock {
	private static final Logger LOGGER = LogManager.getLogger(EventFaceBlock.class);

	@SubscribeEvent
	public void onFacingBlockWithBuildingPlan(DrawBlockHighlightEvent event) {
		Entity entity = event.getInfo().getRenderViewEntity();
		if (entity instanceof PlayerEntity) {
			PlayerEntity player = (PlayerEntity) entity;
			IWorld world = player.getEntityWorld();
			ItemStack stack = player.getHeldItemMainhand();
			RayTraceResult r = event.getTarget();
			if (stack != null) {
				Item item = stack.getItem();
				if (item != null && item.getClass().equals(ItemConstruction2.class)) {
					if (r != null) {
						BlockPos pos = new BlockPos(r.getHitVec()).down();
						double y = r.getHitVec().getY();
						y = (y * 100) % 100;
						if (y > 0d) {
							pos = pos.up();
						}
						if (pos != null) {
							if (!world.getBlockState(pos).isAir(world, pos)) {
								Vec3d playerPos = player.getPositionVector();
								double deltaX = pos.getX() - playerPos.x;
								double deltaY = pos.getY() - playerPos.y;
								double deltaZ = pos.getZ() - playerPos.z;
								// LOGGER.info("### DELTA x={}, y={}, z={}", deltaX, deltaY, deltaZ);
								ItemConstruction2 i2 = (ItemConstruction2) item;
								ConstructionBean bean = i2.getConstructionBeanFromNbt(player, stack);
								Map<EnumExtreme, BlockPos> boundaries = bean.getBoundaries();
								BlockPos max = boundaries.get(EnumExtreme.MAX);
								BlockPos min = boundaries.get(EnumExtreme.MIN);
								Float maxX = (float) max.getX() + 1;
								Float minX = (float) min.getX();
								Float maxY = (float) max.getY() + 1;
								Float minY = (float) min.getY() + 0.02f;
								Float maxZ = (float) max.getZ() + 1;
								Float minZ = (float) min.getZ();
								// draw
								GL11.glPushMatrix();
								GL11.glPushAttrib(GL11.GL_CURRENT_BIT);
								GL11.glLineWidth(3.0f);
								GL11.glColor4f(1.0f, 0.0f, 1.0f, 0.1f);
								GL11.glTranslated(deltaX, deltaY - 0.6f, deltaZ);
								GlStateManager.disableTexture();
								GL11.glBegin(GL11.GL_QUADS);
								GL11.glVertex3f(minX, minY, minZ);
								GL11.glVertex3f(minX, minY, maxZ);
								GL11.glVertex3f(maxX, minY, maxZ);
								GL11.glVertex3f(maxX, minY, minZ);
								GL11.glEnd();
								GL11.glBegin(GL11.GL_LINE_LOOP);
								GL11.glVertex3f(minX, maxY, minZ);
								GL11.glVertex3f(minX, maxY, maxZ);
								GL11.glVertex3f(maxX, maxY, maxZ);
								GL11.glVertex3f(maxX, maxY, minZ);
								GL11.glEnd();
								GL11.glBegin(GL11.GL_LINE_LOOP);
								GL11.glVertex3f(minX, minY, minZ);
								GL11.glVertex3f(minX, maxY, minZ);
								GL11.glVertex3f(maxX, maxY, minZ);
								GL11.glVertex3f(maxX, minY, minZ);
								GL11.glEnd();
								GL11.glBegin(GL11.GL_LINE_LOOP);
								GL11.glVertex3f(minX, minY, maxZ);
								GL11.glVertex3f(minX, maxY, maxZ);
								GL11.glVertex3f(maxX, maxY, maxZ);
								GL11.glVertex3f(maxX, minY, maxZ);
								GL11.glEnd();
								GL11.glBegin(GL11.GL_LINE_LOOP);
								GL11.glVertex3f(minX, minY, minZ);
								GL11.glVertex3f(minX, minY, maxZ);
								GL11.glVertex3f(minX, maxY, maxZ);
								GL11.glVertex3f(minX, maxY, minZ);
								GL11.glEnd();
								GL11.glBegin(GL11.GL_LINE_LOOP);
								GL11.glVertex3f(maxX, minY, minZ);
								GL11.glVertex3f(maxX, minY, maxZ);
								GL11.glVertex3f(maxX, maxY, maxZ);
								GL11.glVertex3f(maxX, maxY, minZ);
								GL11.glEnd();
								GlStateManager.enableTexture();
								GL11.glPopAttrib();
								GL11.glPopMatrix();
							}
						}
					}
				}
			}
		} else {
			LOGGER.warn("Rendering error: found entity is not a player entity");
		}
	}
}
