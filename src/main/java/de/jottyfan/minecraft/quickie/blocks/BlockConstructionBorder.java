package de.jottyfan.minecraft.quickie.blocks;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import de.jottyfan.minecraft.quickie.QuickieMod;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.storage.loot.LootContext.Builder;

/**
 * 
 * @author jotty
 *
 */
public class BlockConstructionBorder extends Block {

	public BlockConstructionBorder(Properties properties) {
		super(properties);
		super.setRegistryName(QuickieMod.MODID, "constructionborder");
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	public int quantityDropped(Random random) {
		return 1;
	}

	@Override
	public List<ItemStack> getDrops(BlockState blockState, Builder builder) {
		return Arrays.asList(new ItemStack[] { new ItemStack(Item.BLOCK_TO_ITEM.get(this)) });
	}
}
