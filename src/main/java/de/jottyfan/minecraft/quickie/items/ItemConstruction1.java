package de.jottyfan.minecraft.quickie.items;

import java.util.List;

import de.jottyfan.minecraft.quickie.QuickieMod;
import de.jottyfan.minecraft.quickie.items.construction.ConstructionBean;
import de.jottyfan.minecraft.quickie.util.QuickieItems;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

/**
 * 
 * @author jotty
 *
 */
public class ItemConstruction1 extends Item {
	private BlockPos startingPos;
	private BlockPos endingPos;

	public ItemConstruction1() {
		super((new Item.Properties()).group(ItemGroup.REDSTONE).maxStackSize(1));
		super.setRegistryName(QuickieMod.MODID, "construction1");
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		if ((stack != null) && (stack.getTag() != null)) {
			BlockPos pos = BlockPos.fromLong(stack.getTag().getLong("startingPos"));
			if (tooltip != null) {
				tooltip.add(new StringTextComponent(pos == null ? "ERROR" : pos.toString()));
			}
		}
	}

	public BlockPos getStartingPosFromNbt(ItemStack stack) {
		CompoundNBT nbt = stack.getTag();
		return nbt == null ? null : BlockPos.fromLong(nbt.getLong("startingPos"));
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		startingPos = getStartingPosFromNbt(playerIn.getHeldItem(handIn));
		if (startingPos != null) {
			String x = String.valueOf(endingPos.getX());
			String y = String.valueOf(endingPos.getY());
			String z = String.valueOf(endingPos.getZ());
			ITextComponent msg = new TranslationTextComponent("msg.buildingplan.end", x, y, z);
			if (worldIn.isRemote) {
				playerIn.sendMessage(msg);
			}
		} else {
			ITextComponent msg = new TranslationTextComponent("msg.buildingplan.null");
			if (worldIn.isRemote) {
				playerIn.sendMessage(msg);
			}
		}
		// building plans are not stackable, so we need not dropping them here
		if (startingPos != null && endingPos != null) {
			ConstructionBean cBean = new ConstructionBean(worldIn, startingPos, endingPos);
			ItemStack newStack = new ItemStack(QuickieItems.ITEM_CONSTRUCTION2);
			byte[] constructionBeanByteArray = cBean.serialize();
			CompoundNBT nbt = new CompoundNBT();
			nbt.putByteArray("constructionBean", constructionBeanByteArray);
			newStack.setTag(nbt);
			playerIn.setHeldItem(handIn, newStack);
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public ActionResultType onItemUse(ItemUseContext ctx) {
		endingPos = ctx.getPos();
		return super.onItemUse(ctx);
	}
}
