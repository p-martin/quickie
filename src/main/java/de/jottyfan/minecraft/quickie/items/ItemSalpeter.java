package de.jottyfan.minecraft.quickie.items;

import de.jottyfan.minecraft.quickie.QuickieMod;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

/**
 * 
 * @author jotty
 *
 */
public class ItemSalpeter extends Item {

	public ItemSalpeter() {
		super((new Item.Properties()).group(ItemGroup.REDSTONE));
		super.setRegistryName(QuickieMod.MODID, "salpeter");
	}
}
