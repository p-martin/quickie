package de.jottyfan.minecraft.quickie.items;

/**
 * 
 * @author jotty
 *
 */
public class ConstructionException extends Exception {
	private static final long serialVersionUID = 1L;

	public ConstructionException(String message) {
		super(message);
	}
}
