package de.jottyfan.minecraft.quickie.proxy;

import de.jottyfan.minecraft.quickie.event.client.EventFaceBlock;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.eventbus.api.IEventBus;

/**
 * 
 * @author jotty
 *
 */
@OnlyIn(Dist.CLIENT)
public class ClientProxy implements IProxy {
	@Override
	public void registerCustomModel(Item item, int metadata, ResourceLocation registryName) {
		// ModelLoader.setCustomModelResourceLocation(item, metadata,
		// new ModelResourceLocation(registryName, "inventory"));
	}

	@Override
	public void registerClientEventFaceBlock(IEventBus bus) {
		bus.register(new EventFaceBlock());
	}
}
