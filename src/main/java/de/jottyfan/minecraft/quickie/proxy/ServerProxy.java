package de.jottyfan.minecraft.quickie.proxy;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.eventbus.api.IEventBus;

/**
 * 
 * @author jotty
 *
 */
@OnlyIn(Dist.DEDICATED_SERVER)
public class ServerProxy implements IProxy {
	@Override
	public void registerCustomModel(Item item, int metadata, ResourceLocation registryName) {
		// nothing to do on server side
	}

	@Override
	public void registerClientEventFaceBlock(IEventBus bus) {
		// nothing to do here
	}
}
