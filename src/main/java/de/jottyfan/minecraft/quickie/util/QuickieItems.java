package de.jottyfan.minecraft.quickie.util;

import de.jottyfan.minecraft.quickie.QuickieMod;
import de.jottyfan.minecraft.quickie.items.ItemConstruction0;
import de.jottyfan.minecraft.quickie.items.ItemConstruction1;
import de.jottyfan.minecraft.quickie.items.ItemConstruction2;
import de.jottyfan.minecraft.quickie.items.ItemLevelup;
import de.jottyfan.minecraft.quickie.items.ItemPencil;
import de.jottyfan.minecraft.quickie.items.ItemSalpeter;
import de.jottyfan.minecraft.quickie.items.ItemSpeedpowder;
import de.jottyfan.minecraft.quickie.items.ItemSulphor;
import de.jottyfan.minecraft.quickie.items.ToolSpeedpowderAxe;
import de.jottyfan.minecraft.quickie.items.ToolSpeedpowderPickaxe;
import de.jottyfan.minecraft.quickie.items.ToolSpeedpowderShovel;
import net.minecraft.item.Item;
import net.minecraftforge.registries.ObjectHolder;

/**
 * 
 * @author jotty
 *
 */
@ObjectHolder(QuickieMod.MODID)
public class QuickieItems {
	public static final ToolSpeedpowderAxe AXE_SPEEDPOWDER = new ToolSpeedpowderAxe();
	public static final ToolSpeedpowderPickaxe PICKAXE_SPEEDPOWDER = new ToolSpeedpowderPickaxe();
	public static final ToolSpeedpowderShovel SHOVEL_SPEEDPOWDER = new ToolSpeedpowderShovel();

	public static final ItemSpeedpowder ITEM_SPEEDPOWDER = new ItemSpeedpowder();
	public static final ItemSulphor ITEM_SULPHOR = new ItemSulphor();
	public static final ItemSalpeter ITEM_SALPETER = new ItemSalpeter();

	public static final ItemConstruction0 ITEM_CONSTRUCTION0 = new ItemConstruction0();
	public static final ItemConstruction1 ITEM_CONSTRUCTION1 = new ItemConstruction1();
	public static final ItemConstruction2 ITEM_CONSTRUCTION2 = new ItemConstruction2();
	public static final ItemPencil ITEM_PENCIL = new ItemPencil();
	public static final ItemLevelup ITEM_LEVELUP = new ItemLevelup();

	public static final Item[] ITEMS = { AXE_SPEEDPOWDER, PICKAXE_SPEEDPOWDER, SHOVEL_SPEEDPOWDER, ITEM_SPEEDPOWDER,
			ITEM_SULPHOR, ITEM_SALPETER, ITEM_CONSTRUCTION0, ITEM_CONSTRUCTION1, ITEM_CONSTRUCTION2, ITEM_PENCIL,
			ITEM_LEVELUP };
}
